const http = require('http');
const port = 3000;


let profile = [
      {
                "firstName": "Mary Jane",
                "lastName": "Dela Cruz",
                "mobileNo": "09123456789",
                "email": "mjdelacruz@mail.com",
                "password": 123
        },
        {
                "firstName": "John",
                "lastName": "Doe",
                "mobileNo": "09123456789",
                "email": "jdoe@mail.com",
                "password": 123
        }
]


const server = http.createServer((req, res)=>{

if(req.url == '/profile' && req.method =="GET"){
	res.writeHead(200,{'Content-Type':'application/json'})
	res.write(JSON.stringify(profile));
	res.end()
}

if(req.url == '/profile' && req.method =="POST"){
	//Declare and initialize a requestBody variable to an empty string
	//This will act as placeholder for the resource/data to be created later on
	let requestBody = '';
  // A 'stream' is a sequence of data
  //A data step - this is the first sequence of stream. This reads the "data" stream and process it as the request body.
  //The information provided from the request object (client) enters a sequence of "data"

  req.on('data', function(data){
  	//assigns the data rectrieved from the data stream to requestBody 
  	requestBody += data;

  });
  //response end step - only runs after the request has completely been sent
  req.on('end',function(){
  	console.log(typeof requestBody);
  	//Converts the string requestBody to JSON
  	requestBody = JSON.parse(requestBody);
  	//Create new obj representing the new mock database record
  	let newUser = {
  		"firstName":requestBody.firstName,
  		"lastName":requestBody.lastName,
        "mobileNo": requestBody.mobileNo,
        "email": requestBody.email,
        "password": requestBody.password
  	}
  	//Add the new user into the mock database
  	profile.push(newUser);
  	console.log(profile);
  	res.writeHead(200,{'Content-Type':'application/json'});
  	res.write(JSON.stringify(newUser));
  	res.end();
  })
}






});
server.listen(port);


console.log(`Server running at localhost: ${port}`);